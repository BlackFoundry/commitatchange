Commit At Change
==============

This script adds observers when the > (play) button is pressed.
While running, every modification of the font will trigger a commit to the local git repository.
The 'Push' button allows you to push the commits on the server.