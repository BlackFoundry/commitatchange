import subprocess
import os
import threading
import Queue
import xml.etree.ElementTree as ET
from vanilla import *
from mojo.events import addObserver, removeObserver
from AppKit import (NSImage, NSImageNameRightFacingTriangleTemplate,
                    NSImageNameStopProgressTemplate)
from time import time

imStart = NSImage.imageNamed_(NSImageNameRightFacingTriangleTemplate)
imStart.setSize_((10, 10))

imStop = NSImage.imageNamed_(NSImageNameStopProgressTemplate)
imStop.setSize_((10, 10))

LEVEL = ""
start = 0


def timeDec(func):
    def wrapper(*args, **kwargs):
        global LEVEL
        global start
        start = time()
        LEVEL += "\t"
        ret = func(*args, **kwargs)
        LEVEL = LEVEL[:-1]
        elapsed = time() - start
        # if elapsed > 0.2:
        #     print(LEVEL + func.__name__ + str(elapsed))
        return ret
    return wrapper


def MobileWorker(queue):
    """TK-53 CGS"""
    while True:
        commandDict = queue.get(True)
        command = commandDict["command"]
        cwd = commandDict["cwd"]
#        print("EXEC: " + str(command))
        subprocess.Popen(command, cwd=cwd, stdout=subprocess.PIPE)


class commitAtChange(object):

    def __init__(self):
        self.queue = Queue.Queue()
        self.workerThread = threading.Thread(
            target=MobileWorker, args=(self.queue,))
        self.workerThread.daemon = True
        self.workerThread.start()

        self.commitsCount = {}
        if AllFonts() == []:
            return
        for font in AllFonts():
            head, tail = os.path.split(font.path)
            fontName = font.info.familyName + '-' + font.info.styleName
            self.getRepoBase(font.path)
            if self.repoBase is not None:
                self.commitsCount[fontName] = self.getNumberOfUnpushedCommits(
                    self.repoBase)
            else:
                self.commitsCount[fontName] = 0
        self.repoBase = None
        self.font = None
        self.fontName = ''
        font = CurrentFont()
        if font is not None:
            self.font = font
            head, tail = os.path.split(self.font.path)
            self.path = head
            self.fontName = (
                self.font.info.familyName + '-' + self.font.info.styleName)
            self.getRepoBase(font.path)

        self.title = "Commit At Change"
        self.w = FloatingWindow((150, 120), self.title, closable=False)
        self.w.titleTextBox = TextBox(
            (10, 10, -10, 20),
            self.fontName, alignment='center', sizeStyle='mini')
        self.w.startButton = ImageButton(
            (10, 30, -10, 20), imageObject=imStart,
            callback=self.startSaveObserver, sizeStyle="small")
        self.w.startButton.getNSButton().setBordered_(False)
        self.w.stopButton = ImageButton(
            (10, 30, -10, 20), imageObject=imStop,
            callback=self.stopSaveObserver, sizeStyle="small")
        self.w.stopButton.getNSButton().setBordered_(False)
        buttonTitle = "Push %s" % str(self.commitsCount[self.fontName])
        self.w.pushButton = Button(
            (10, 60, -10, 20), buttonTitle,
            callback=self.gitPush, sizeStyle="small")
        self.w.closeButton = Button(
            (10, -30, -10, 20), "Close",
            callback=self.closeWindow, sizeStyle="small")
        self.w.stopButton.show(False)
        self.w.pushButton.show(
            self.repoBase is not None and self.font is not None and
            self.commitsCount[self.fontName] != 0)
        addObserver(self, "currentFontChanged", "fontBecameCurrent")
        addObserver(self, "newFontOpened", "fontDidOpen")
        self.w.open()

    @timeDec
    def getRepoBase(self, path):
        self.repoBase = None
        try:
            repoBase = subprocess.check_output(
                ['git', 'rev-parse', '--show-toplevel'], cwd=path)
            while repoBase[-1] == '\n':
                repoBase = repoBase[:-1]
            self.repoBase = repoBase
        except:
            self.repoBase = None

    @timeDec
    def getNumberOfUnpushedCommits(self, repoBase):
        count = 0
        c = subprocess.check_output(['git', 'cherry', '-v'], cwd=repoBase)
        lines = c.split('\n')
        return len(lines)

    @timeDec
    def updateTitle(self):
        nbCommits = str(self.commitsCount[self.fontName])
        self.w.titleTextBox.set(self.fontName)
        self.w.pushButton.setTitle("Push " + nbCommits)

    @timeDec
    def currentFontChanged(self, info):
        self.font = info['font']
        head, tail = os.path.split(self.font.path)
        self.path = head
        self.fontName = (self.font.info.familyName +
                         '-' + self.font.info.styleName)
        self.updateTitle()
        self.getRepoBase(self.path)
        if self.repoBase is None:
            self.stopSaveObserver(None)
            self.w.pushButton.show(False)
        else:
            self.getNumberOfUnpushedCommits(self.repoBase)
        self.updateTitle()

    @timeDec
    def newFontOpened(self, font):
        for font in AllFonts():
            fontName = font.info.familyName + '-' + font.info.styleName
            self.getRepoBase(font.path)
            if self.repoBase is not None:
                self.commitsCount[fontName] = self.getNumberOfUnpushedCommits(
                    self.repoBase)
            else:
                self.commitsCount[fontName] = 0
                self.w.pushButton.show(False)

    @timeDec
    def startSaveObserver(self, sender):
        if self.repoBase is None:
            return
        self.w.startButton.show(False)
        self.w.stopButton.show(True)
        self.font.addObserver(self, "commitWhenChanged", "Font.Changed")

    @timeDec
    def stopSaveObserver(self, sender):
        self.w.startButton.show(True)
        self.w.stopButton.show(False)
        self.font.removeObserver(self, "Font.Changed")

    @timeDec
    def closeWindow(self, sender):
        try:
            self.font.removeObserver(self, "Font.Changed")
        except:
            pass
        removeObserver(self, "fontBecameCurrent")
        removeObserver(self, "fontDidOpen")
        self.w.close()

    @timeDec
    def getGlyphNameformGLIF(self, repoBase, glifFile):
        filePath = os.path.join(repoBase, glifFile)
        try:
            tree = ET.parse(filePath)
            root = tree.getroot()
            return root.get('name')
        except:
            return ''

    @timeDec
    def commitWhenChanged(self, Notification):
        if not self.repoBase:
            self.stopSaveObserver(None)
            return
        self.font.removeObserver(self, "Font.Changed")
        self.font.save()
        self.font.addObserver(self, "commitWhenChanged", "Font.Changed")
        global start

#        print(LEVEL + 'ANALYSING CHANGES')
        glyphsChanged = []
        log = subprocess.check_output(
            ['git', 'log', '-1', '--pretty=format:', '--name-only'],
            cwd=self.repoBase)
        for l in log.split('\n'):
            if l.endswith('.glif'):
                glifName = self.getGlyphNameformGLIF(self.repoBase, l)
                glyphsChanged.append(glifName)
        plural = ''
        if len(glyphsChanged) > 1:
            plural = 's'
        comment = "Glyph%s '%s' Edited in Font '%s'" % (
            plural, " ".join(glyphsChanged), self.fontName)
        # print(LEVEL + 'ANALYSING CHANGES elapsed ' + str(tbf))
        self.queue.put(
            {"command": ['git', 'add', '-f', self.path], 'cwd': self.path})
        self.queue.put(
            {"command": ['git', 'commit', '-am', comment], 'cwd': self.path})
        self.commitsCount[
            self.fontName] = self.getNumberOfUnpushedCommits(self.repoBase)
        self.w.pushButton.show(self.path is not None and self.font is not None)
#        self.w.pushButton.setTitle(
#            'Push ' + str(self.commitsCount[self.fontName]))
        self.updateTitle()
        return

    @timeDec
    def gitPush(self, sender):
        self.queue.put({"command": ['git', 'push'], 'cwd': self.path})
        self.commitsCount[self.fontName] = 0
        self.w.pushButton.show(False)
        self.updateTitle()

commitAtChange()
